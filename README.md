# kimun-helm

fist steps in helm

* [How to install](https://helm.sh/docs/intro/install/)


# Commands
* `helm version` - helm version
* `helm create <chart-name>`  - create one chart proyect
* `helm upgrade -i <chart name> .` - install or upgrade one chart
* `helm --namespace=default upgrade -i $APP-NAME --set api_url=$API_URL --set image.repository=$NEXUS_HOST/$APP-NAME --set image.tag=1.0.0-SNAPSHOT --debug -f values.yaml .` - install or upgrade with non default parameters
* `helm uninstall <chat-name>`- tearing down
* `helm list` - list of instances
* `helm ls` -  list of instances

* `helm lint` - Troubleshooting trick - validate the sintax in the configuration
* `helm template .` - Troubleshooting trick - show you how the template with the values is going to be apply (demo)
* `helm template . > templeted.yaml` - Troubleshooting trick - show you how the template with the values is going to be apply (demo), you can use an editor to check the missing values
* `helm template . | kubectl create -f -` - Troubleshooting trick - simulate the deploy, we can see the missing values or the errors in the template
* `helm package .` - pkg one helm chart project

# Other usefull commands
* `kubectl get deployments` - list of deployments
* `kubectl get svc` -  list of services
* `minikube ip` - ip of minikube
* `curl {mikube-ip}:{service-port}` - testing locally one particular WS deploy in minikube

* `kubectl get pods` - list of pods
* `kubectl describe pod <pod-name>` - information about one particular pod (status)
* `kubectl log <pod-name>` - log of <pod-name>
* `kubectl create secret docker-registry nexus-dev  --docker-server=nexus3.uchile.cl:8085  --docker-username=**** --password=****   --docker-email=leonardobravo@uchile.cl` - create one secret lo login with nexus

# Helm - Flow Control
* [Helm control control_structures doc](https://helm.sh/docs/chart_template_guide/control_structures/)
* [Helm operators and functions](https://helm.sh/docs/chart_template_guide/functions_and_pipelines/#operators-are-functions)

You can define some extra configuration on the deployment, with some conditions, form example the envirorment where you are traying to deploy the application has some extra parameters.
To do this you need create a template with the extra configuration and with some flow controls in the deployment.yaml you can handel autmatically the additions of that filds, using  one particular value fild, like the envirorment or other define in values.yaml .
For example:
* [Deploymen.yaml](https://gitlab.com/l30bravo/kimun-helm/-/blob/master/templatedchart/templates/deployment.yaml#L23-25) - has a flow control to add some proxy configuration, depends on `Values.proxy.enable true `
* [_proxy.tpl](https://gitlab.com/l30bravo/kimun-helm/-/blob/master/templatedchart/templates/_proxy.tpl) - extra configuration to be apply


# Chart Museum
Its a repository for charts.
You can install on prem, package your charts , upload and share with other developers
* [Chart Museum](https://chartmuseum.com/)
* [helm/chartmuseum](https://github.com/helm/chartmuseum)
* [helm/chartmuseum#helm-chart](https://github.com/helm/chartmuseum#helm-chart)
* [Fail to upload chart to chartmuseum](https://stackoverflow.com/questions/48577211/fail-to-upload-chart-to-chartmuseum)

1. `helm repo add chartmuseum https://chartmuseum.github.io/charts`
2. `helm install chartmuseum/chartmuseum`
3. edit in values `DISABLE_API:false`
4. edit in values `service.type: NodePort`
5. edit in values `service.nodePort: 30001`
6. `helm install chartm .` (in the folder of chartmuseum)
7. `minikube ip` - get server ip
8. in a web browser `{minikube-ip}:30001`
9. adding a repo `helm repo add myrepo http://{minikube-ip}:30001`
10. `helm repo list`
11. `helm repo update`
12. to pkg one chart project: `helm package .` - into the project
13. `curl --data-binary "@{my-chart}.tgz" http://{minikube-ip}:30001/api/charts`; output: `{"saved":true}`
14. `helm repo update`
15. `helm search repo {name-of-the-chart}` - search one chart in one repo
16. `helm install {chart-name} {name-from-repo/chart}` - install one chart from chart museum
16. `helm install {chart-name} {name-from-repo/chart} --version {number}` - install one chart from chart museum, one particular version
17. `helm list`- to validate that the las chart it's in the list
18. `helm uninstall {chart-name}` - to remove
19. `curl -X DELETE http://{minikube-ip}:30001/api/charts/{chart-name}/{version}` - remove one particular pkg in the repository
20. `helm repo update`
21. `helm repo list`
22. `helm repo remove {repo-name}` - delete one repository

# Nexus repository Helm Format
* [Nexus Repository Helm Format](https://github.com/sonatype-nexus-community/nexus-repository-helm)
* [Sonatype Helm Repositories](https://help.sonatype.com/repomanager3/formats/helm-repositories)

# Umbrella Chart
* Charts within a chart.
* What we can do here is with this values fall inside umbrella is we can override values inside the sub-chart.

* [Umbrella example](https://gitlab.com/l30bravo/kimun-helm/-/tree/master/umbrella)
* [Replace values from internal chart](https://gitlab.com/l30bravo/kimun-helm/-/blob/master/umbrella/values.yaml#L13-19)
* [Global Values -1](https://gitlab.com/l30bravo/kimun-helm/-/blob/master/umbrella/values.yaml#L5-11)
* [Global Values -2](https://gitlab.com/l30bravo/kimun-helm/-/blob/master/umbrella/templates/service.yaml#L4-11)

# Dependencies
* `helm create dep`
* `apiVersion: v2` => helm 3.X
* adding dependencies [example](https://gitlab.com/l30bravo/kimun-helm/-/blob/master/dep/requirements.yaml)
* some usefull commands:
1. `helm repo update`
2. `helm repo list`
3. `helm search repo tomcat`
* how to use dependencies
4. into dep `helm dep list` - is missing
5. `hel dep build`, this pull down the dsependencies.
6.

# Some concepts


    * ClusterIP: Exposes the service on a cluster-internal IP. Choosing this value makes the service only reachable from within the cluster. This is the default ServiceType

    * NodePort: Exposes the service on each Node’s IP at a static port (the NodePort). A ClusterIP service, to which the NodePort service will route, is automatically created. You’ll be able to contact the NodePort service, from outside the cluster, by requesting <NodeIP>:<NodePort>.

    * LoadBalancer: Exposes the service externally using a cloud provider’s load balancer. NodePort and ClusterIP services, to which the external load balancer will route, are automatically created.

# Links
* [What is Helm in Kubernetes? Helm and Helm Charts explained](https://youtu.be/-ykwb1d0DXU)
* [What is Helm Charts | Helm Kubernetes Demo with NGINX](https://youtu.be/j-YBgTnV2v0)
* [helm.sh](https://helm.sh/)
* [Helm Hub](https://artifacthub.io/)
* [Quickstart](https://helm.sh/docs/intro/quickstart/)
* [Helm masterclass](https://www.udemy.com/course/helm-masterclass/)
* [Helm Course](https://github.com/addamstj/helm-course)
